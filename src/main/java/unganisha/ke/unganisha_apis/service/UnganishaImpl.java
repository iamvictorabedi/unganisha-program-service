package unganisha.ke.unganisha_apis.service;

import java.util.List;

import org.springframework.stereotype.Service;
import unganisha.ke.unganisha_apis.domains.Account;
import unganisha.ke.unganisha_apis.repository.AccountRepo;
import unganisha.ke.unganisha_apis.repository.RequestRepo;
import unganisha.ke.unganisha_apis.repository.SecurityRepo;

@Service
public class UnganishaImpl {
    private AccountRepo accountRepo;
    private RequestRepo requestRepo;
    private SecurityRepo securityRepo;

    public UnganishaImpl(AccountRepo accountRepo, RequestRepo requestRepo, SecurityRepo securityRepo) {
        this.accountRepo = accountRepo;
        this.requestRepo = requestRepo;
        this.securityRepo = securityRepo;
    }

    public List<Account> findAll() {
        return accountRepo.findAll();
    }
}
