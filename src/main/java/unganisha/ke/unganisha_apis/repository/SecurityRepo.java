package unganisha.ke.unganisha_apis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import unganisha.ke.unganisha_apis.domains.Security;

public interface SecurityRepo extends JpaRepository<Security,Long> {
}
