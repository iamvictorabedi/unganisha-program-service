package unganisha.ke.unganisha_apis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import unganisha.ke.unganisha_apis.domains.Request;

public interface RequestRepo  extends JpaRepository<Request,Long>{
}
