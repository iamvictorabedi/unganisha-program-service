package unganisha.ke.unganisha_apis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import unganisha.ke.unganisha_apis.domains.Account;

public interface AccountRepo extends JpaRepository<Account,Long> {
}
