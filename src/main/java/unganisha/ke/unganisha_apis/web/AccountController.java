package unganisha.ke.unganisha_apis.web;

import java.util.List;
import javax.servlet.annotation.MultipartConfig;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import unganisha.ke.unganisha_apis.domains.Account;
import unganisha.ke.unganisha_apis.service.UnganishaImpl;

@RestController
@MultipartConfig(maxFileSize = 5*1024*1024,maxRequestSize = 10*1024*1024,fileSizeThreshold = 5*1024*1024)
@RequestMapping(path = "account", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class AccountController {

    private final UnganishaImpl unganisha;

    public AccountController(UnganishaImpl unganisha) {
        this.unganisha = unganisha;
    }

    // Get All Notes
    @GetMapping("")
    public List<Account> getAllAccounts() {
        return unganisha.findAll();
    }
}
