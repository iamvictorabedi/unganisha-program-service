package unganisha.ke.unganisha_apis.web;

import javax.servlet.annotation.MultipartConfig;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import unganisha.ke.unganisha_apis.service.UnganishaImpl;

@RestController
@MultipartConfig(maxFileSize = 5*1024*1024,maxRequestSize = 10*1024*1024,fileSizeThreshold = 5*1024*1024)
@RequestMapping(path = "security", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class SecurityController {

    private final UnganishaImpl unganisha;

    public SecurityController(UnganishaImpl unganisha) {
        this.unganisha = unganisha;
    }
}
